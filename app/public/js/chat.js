document.addEventListener("DOMContentLoaded", function (event) {
  //Nos conectamos al servicio de socket
  const socket = io("http://10.200.242.213:2000");

  /*
   * Acciones que se realizan cuando se establece conexión con el servidor de socket.
   * tambien introducimos al usuario en la sala que corresponde
   */
  socket.on("connected", (data) => {
    console.log(data.msg); 
  });

  /*
   * Acciones que se realizarán cuando otro usuario envia mensaje
   */
  socket.on("toChat", (data) => {
        console.log(data);
        var chatBox = document.getElementById("chat");
        chatBox.innerHTML += 'Patata: ' + data.text + '<br>';
  });

  /**
   * Acciones cuando se pulsa el botón de "Enviar"
   */
  document.getElementById("send").addEventListener("submit", (e) => {
    e.preventDefault();
    var msgInput = document.getElementById("msg");
    var chatBox = document.getElementById("chat");
    var msg = msgInput.value;
  });
  //Accion a realizar cuando se da al boton de enviar
  document.getElementById("send").addEventListener("submit", (e) => {
    //Obtenemos todos los elementos del formulario para trabajar con ellos
    e.preventDefault();
    var msgInput = document.getElementById("msg");
    var chatBox = document.getElementById("chat");
    var msg = msgInput.value;

    //Mostramos el mensaje en la ventana para el usuario que lo envia
    chatBox.innerHTML += `Patata: ${msgInput.value}<br>`;

    // Definimos el mensaje que vamos a enviar
    var toSend = { user: "Patata", text: msg };

    //Enviamos el mensaje al servidor utilizando el evento "broadcast" definido por nosotros
    socket.emit("broadcast", toSend);
    //socket.on("broadcast", toSend);
  });
});
